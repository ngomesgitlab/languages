## Title

  Countries Official Languages

## Technical Environment Rules

    Please implement the API, and skip UI. It’s also fine to skip the authentication.
    The purpose is not to build a solid API but for you to demonstrate your coding skills, architecture and engineering practices.

    Goal

    Image you have a set of data in JSON, describing official languages spoken by countries, as
    such:

        [
            {
                country:"US",
                languages: ["en"]
            },
            {
                country:"BE",
                languages: ["nl","fr","de"]
            },
            {
                country:"NL",
                languages: ["nl","fy"]
            },
            {
                country:"DE",
                languages: ["de"]
            },
            {
                country:"ES",
                languages: ["es"]
            }
        ]

    Write a function in Java that:

    - returns the number of countries in the world
    - finds the country with the most official languages, where they officially speak German (de).
    - that counts all the official languages spoken in the listed countries.
    - to find the country with the highest number of official languages.
    - to find the most common official language(s), of all countries.

    Writing tests!

## Tooling

  1. Please install IntelliJ or Eclipse Lombok plugin, to avoid possible compilation problems;
  2. Java 11;
  3. Maven;
  4. This application needs following ports available : 8088

## Tech Stack

  1. Spring Framework

      - Spring Boot, Spring Web, Sprint Test
      - Please check POM for more information

  2. Lombok Plugin :

      - Reduce Boiler Plate code

  3. CheckStyle :

      - Code style pattern plugin (checkstyle.xml)

  4. Swagger :

      - Application and Endpoint Documentation and Testing

# First Steps

    1. You can start the application running : mvn spring-boot:run
    2. After start the application please go to SWAGGER UI Home Page (http://localhost:8080/swagger-ui.html) and read application rules, and information.
    3. You are also going to be able to test all application features from this Home Page.
    4. For a better user experience please take some minutes to read our instructions before start using it.

## Tests

  1. Unit and Integration Tests :

    - Folder : Src -> Test -> Java -> official.languages -> unittests

  2. Integration Tests (Test End To End Calling EndPoint Using Apache Connector)

    - Folder : Src -> Test -> Java -> official.languages -> integrationtests

  3. Tests Support Classes :

    - Folder : Src -> Test -> Java -> official.languages -> testsupport


## GIT Lab

  If you want to login in to validate the project in GITLab you can use this login / password :

    1 - User : ngomesgitlab
    2 - Password : ngomesgitlab@1234