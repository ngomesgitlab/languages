package official.languages.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import official.languages.common.predicates.GenericPredicates;
import official.languages.common.validations.BindingResultValidation;
import official.languages.data.CountryLanguagesDTO;
import official.languages.data.ResponseDTO;
import official.languages.services.interfaces.IOfficialLanguagesServices;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Set;

import static official.languages.common.constants.CountryLanguagesConstants.ERROR_DURING_GENERATED_OFFICIAL_LANGUAGE_REPORT;
import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/officialLanguages")
@Api("Handles Country Official Languages Report Process")
public class OfficialLanguagesController {

    private final IOfficialLanguagesServices officialLanguagesServices;

    @PostMapping(value = "/generateReport")
    @ApiOperation(value = "Generates Country Official Languages Report.",
        notes =
            "Languages Report Rules : " +
            "Based on the list received we are going to validate this points.\n" +
            "a - returns the number of countries in the world.\n" +
            "b - finds the country with the most official languages, where they officially speak German (de).\n" +
            "c - that counts all the official languages spoken in the listed countries.\n" +
            "d - to find the country with the highest number of official languages.\n" +
            "e - to find the most common official language(s), of all countries.\n \n" +
            "Assumptions : \n" +
            "a - If you send a duplicated country, only the first entry will be valid.\n" +
            "c - All Countries must at least speak one language.\n \n" +
            "DTO Rules : \n" +
            "1 - Country : \n" +
            "a - Must have 2 Digits. \n" +
            "b - Cannot be Null or Empty. \n" +
            "c - All Letters must be in uppercase style \n \n" +
            "2 - Languages : \n" +
            "a - Must have 2 Digits. \n" +
            "b - Cannot be Null or Empty. \n" +
            "c - All Letters must be in lowercase style \n")
    @ApiResponses(value = {
    @ApiResponse(code = 200, message = "Success"),
    @ApiResponse(code = 404, message = "Not found"),
    @ApiResponse(code = 401, message = "Unauthorised"),
    @ApiResponse(code = 500, message = "Internal server error")
    })
    public ResponseEntity<Object> getOfficialLanguagesReport(
        @ApiParam(required = true) @Valid @RequestBody Set<CountryLanguagesDTO> countryLanguagesDTOSet, BindingResult bindingResult) {

        try {

            //Validate DTO Spring Annotation Errors
            ResponseEntity<ResponseDTO> responseResponseEntity = BindingResultValidation.validateBindingResult(bindingResult);
            if (GenericPredicates.checkIfNullOrEmpty.negate().test(responseResponseEntity)) return badRequest().body(responseResponseEntity);

            return ok().body(officialLanguagesServices.getCountryLanguagesReportDTO(countryLanguagesDTOSet));


        } catch (Exception ex) {

            log.error(ERROR_DURING_GENERATED_OFFICIAL_LANGUAGE_REPORT, ex);
            return badRequest().body(ex.getMessage());

        }

    }


}
