package official.languages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ScopedProxyMode;

import javax.validation.Validator;

@ComponentScan(scopedProxy = ScopedProxyMode.INTERFACES)
@SpringBootApplication
public class OfficialLanguagesApplication {

    @Bean
    public Validator validator() {

        return new org.springframework.validation.beanvalidation.LocalValidatorFactoryBean();
    }

    public static void main(String[] args) {
        SpringApplication.run(OfficialLanguagesApplication.class, args);
    }

}
