package official.languages.config.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.RequestHandlerSelectors.withClassAnnotation;

/**
 * Swagger Control Class
 */
@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .paths(PathSelectors.any())
                .apis(withClassAnnotation(RestController.class))
                .build()
                .apiInfo(internalApiInfo())
                .globalResponseMessage(RequestMethod.GET, responseMessage())
                .globalResponseMessage(RequestMethod.POST, responseMessage())
                .globalResponseMessage(RequestMethod.PUT, responseMessage())
                .globalResponseMessage(RequestMethod.DELETE, responseMessage());
    }



    private ApiInfo internalApiInfo() {
        String title = "Country Official Languages API";
        String description =
                        "<p>Country Official Languages API Documentation</p>" +
                        "<p>This API receives a list of countries and their official languages and generate some reports</p>" +
                        "<p>Please validate Endpoints and DTO´s Documentation for more information</p>";
        String version = "0.0.1";
        Contact contact = new Contact("Application Support Team", "https://www.company.com/contact-us", "company@support.com");

        return new ApiInfo(title, description, version, null, contact, null, null,
                Collections.emptyList());
    }


    private List<ResponseMessage> responseMessage() {

        ResponseMessage internalServerError = new ResponseMessageBuilder()
                .code(500)
                .message("Internal server error")
                .build();

        ResponseMessage forbidden = new ResponseMessageBuilder()
                .code(403)
                .message("Forbidden")
                .build();

        ResponseMessage unauthorised = new ResponseMessageBuilder()
                .code(401)
                .message("Unauthorised")
                .build();

        ResponseMessage notFound = new ResponseMessageBuilder()
                .code(404)
                .message("Not found")
                .build();

        ResponseMessage success = new ResponseMessageBuilder()
                .code(200)
                .message("Success")
                .build();

        ResponseMessage invalidRequestBody = new ResponseMessageBuilder()
                .code(422)
                .message("Invalid request body")
                .build();

        return List.of(
                internalServerError,
                forbidden,
                unauthorised,
                notFound,
                success,
                invalidRequestBody);

    }
}
