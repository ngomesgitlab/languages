package official.languages.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryLanguagesDTO {

    @NotNull(message = "Country should not be null")
    @NotEmpty(message = "Country should not be empty")
    @ApiModelProperty(example = "US, PT, BR",
        required = true,
        allowableValues = "ALL COUNTRIES IN THE WORLD",
        notes = "1 - Must have 2 Digits. \n" +
                "2 - Cannot be Null or Empty. \n" +
                "3 - All Letters must be in uppercase style.")
    private String country;

    @NotNull(message = "Language should not be null")
    @NotEmpty(message = "Language should not be empty")
    @ApiModelProperty(example = "[ pt, us, de ]",
        required = true,
            allowableValues = "ALL COUNTRIES OFFICIAL LANGUAGES",
        notes = "1 - Country Official Languages must have exactly 2 Lowercase Letters. \n" +
                "2 - Cannot be Null or Empty. \n" +
                "3 - All Letters must be in lowercase style.")
    private Set<String> languages;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryLanguagesDTO that = (CountryLanguagesDTO) o;
        return Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country);
    }
}
