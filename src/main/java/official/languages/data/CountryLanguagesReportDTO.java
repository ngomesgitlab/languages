package official.languages.data;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryLanguagesReportDTO {

    //A - returns the number of countries in the world
    @ApiModelProperty(notes = "Number of countries in the world.")
    private int numberCountriesInTheWorld;

    //B - finds the country with the most official languages, where they officially speak German (de)
    @ApiModelProperty(
        notes = "1 - Countries with the most official languages, where they officially speak German (de).")
    private List<String> countryMostOfficialLanguagesIncludingGermany;

    //C - that counts all the official languages spoken in the listed countries.
    @ApiModelProperty(
        notes = "Number of all the official languages spoken in the listed countries.")
    private int numberAllOfficialLanguages;

    //D - to find the country with the highest number of official languages
    @ApiModelProperty(
        notes = "Countries with the highest number of official languages.")
    private List<String> countriesMostOfficialLanguage;

    //E - to find the most common official language(s), of all countries
    @ApiModelProperty(
        notes = "The most common official language(s), of all countries.")
    private List<String> mostCommonOfficialLanguage;



}
