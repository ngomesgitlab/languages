package official.languages.common.functions;

import official.languages.common.predicates.GenericPredicates;
import official.languages.data.CountryLanguagesDTO;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static official.languages.common.constants.CountryLanguagesConstants.SPECIFIC_LANGUAGE;

@SuppressWarnings({"squid:S1444","squid:S1104"})
public class CountryLanguagesFunctions {

    private CountryLanguagesFunctions() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    //B - finds the country with the most official languages, where they officially speak German (de)
    public static Function<Set<CountryLanguagesDTO>, List<String>> getCountryWithMostOfficialLanguagesIncludingGermany = countryLanguagesDTOSet -> {

        Integer mostEntriesSize = countryLanguagesDTOSet
            .parallelStream()
            .filter(blankValue -> GenericPredicates.checkIfNullOrEmpty.negate().test(blankValue))
            .filter(x -> x.getLanguages().contains(SPECIFIC_LANGUAGE))
            .max(comparing(x -> x.getLanguages().size()))
            .map(x -> x.getLanguages().size())
            .orElse(0);

        //return Countries With Most Official Languages, where they officially speak German (de)
        return countryLanguagesDTOSet
            .parallelStream()
            .filter(x -> x.getLanguages().contains(SPECIFIC_LANGUAGE))
            .filter(x -> x.getLanguages().size() == mostEntriesSize)
            .map(CountryLanguagesDTO::getCountry)
            .collect(Collectors.toList());

    };


    //C - that counts all the official languages spoken in the listed countries.
    public static ToIntFunction<Set<CountryLanguagesDTO>> countAllOfficialLanguages = countryLanguagesDTOSet ->
        countryLanguagesDTOSet
            .parallelStream()
            .map(CountryLanguagesDTO::getLanguages)
            .flatMap(Collection::parallelStream)
            .collect(Collectors.toSet())
            .size();

    //D - to find the countries with the highest number of official languages
    public static Function<Set<CountryLanguagesDTO>, List<String>> getCountryWithMostOfficialLanguages = countryLanguagesDTOSet -> {

        Integer mostEntriesSize = countryLanguagesDTOSet
            .parallelStream()
            .max(comparing(x -> x.getLanguages().size()))
            .map(x -> x.getLanguages().size())
            .orElse(0);

        //return Countries With Most Official Languages
        return countryLanguagesDTOSet
            .parallelStream()
            .filter(x -> x.getLanguages().size() == mostEntriesSize)
            .map(CountryLanguagesDTO::getCountry)
            .collect(Collectors.toList());

    };

    //E - to find the most common official language(s), of all countries
    public static Function<Set<CountryLanguagesDTO>, List<String>> getMostCommonOfficialLanguage = countryLanguagesDTOSet -> {

        //Transform to a Map with Duplicated Languages (Languages x Num Entries"
        Map<String, Long> languageXCountMap = countryLanguagesDTOSet
            .parallelStream()
            .map(CountryLanguagesDTO::getLanguages)
            .flatMap(Collection::parallelStream)
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        //Get the number of most repetitive languages based on Map Value
        Long maxNumOfAnEntry = languageXCountMap.entrySet()
            .stream()
            .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
            .get()
            .getValue();

        //return Most Common Official Language(s)
        return languageXCountMap.entrySet()
            .stream()
            .filter(entry -> Objects.equals(entry.getValue(), maxNumOfAnEntry))
            .map(Map.Entry::getKey)
            .collect(Collectors.toList());

    };





}
