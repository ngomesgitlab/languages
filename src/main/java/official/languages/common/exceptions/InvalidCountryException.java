package official.languages.common.exceptions;

public class InvalidCountryException extends Exception {

    public InvalidCountryException(String message) {
        super(message);
    }

}
