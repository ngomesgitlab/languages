package official.languages.common.exceptions;

public class InvalidLanguageException extends Exception {

    public InvalidLanguageException(String message) {
        super(message);
    }

}
