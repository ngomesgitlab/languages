package official.languages.common.validations;

import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import official.languages.common.predicates.GenericPredicates;
import official.languages.data.CountryLanguagesDTO;

import java.util.Collection;
import java.util.Set;

import static official.languages.common.constants.CountryLanguagesConstants.COUNTRY_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.ERROR_VALIDATION_COUNTRIES_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.ERROR_VALIDATION_LANGUAGE_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.LANGUAGE_PATTERN;

public class CountryLanguagesPatternValidations {

    private CountryLanguagesPatternValidations() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    public static void validateCountryLanguagesDTOCollection(Set<CountryLanguagesDTO> countryLanguagesDTOSet) throws InvalidCountryException, InvalidLanguageException {

        /**
         * Validate all Countries - Thrown Exception If one is invalid
         */
        validateCountriesEntries(countryLanguagesDTOSet);
        validateLanguagesEntries(countryLanguagesDTOSet);

    }

    private static void validateCountriesEntries(Set<CountryLanguagesDTO> countryLanguagesDTOSet) throws InvalidCountryException {

        if (GenericPredicates.checkIfNullOrEmpty.test(countryLanguagesDTOSet) ||
            countryLanguagesDTOSet
            .stream()
            .anyMatch(
                country -> GenericPredicates.checkIfNullOrEmpty.test(country.getCountry()) ||
                !country.getCountry().matches(COUNTRY_PATTERN))) {
            throw new InvalidCountryException(ERROR_VALIDATION_COUNTRIES_PATTERN);
        }

    }


    private static void validateLanguagesEntries(Set<CountryLanguagesDTO> countryLanguagesDTOSet) throws InvalidLanguageException {

        if (countryLanguagesDTOSet
            .stream()
            .map(CountryLanguagesDTO::getLanguages)
            .flatMap(Collection::stream)
            .anyMatch(lang -> GenericPredicates.checkIfNullOrEmpty.test(lang) ||
                      !lang.matches(LANGUAGE_PATTERN))) {
            throw new InvalidLanguageException(ERROR_VALIDATION_LANGUAGE_PATTERN);
        }

    }

}
