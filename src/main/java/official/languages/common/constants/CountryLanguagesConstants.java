package official.languages.common.constants;

public class CountryLanguagesConstants {

    private CountryLanguagesConstants() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validations Patterns
     */
    public static final String COUNTRY_PATTERN = "^[A-Z]{2}$";
    public static final String LANGUAGE_PATTERN = "^[a-z]{2}$";

    /**
     * Constants
     */
    public static final String SPECIFIC_LANGUAGE = "de";

    /**
     * ERROR MESSAGES
     */
    public static final String ERROR_DURING_GENERATED_OFFICIAL_LANGUAGE_REPORT = "Error during Generated Official Language Report";
    public static final String ERROR_VALIDATION_COUNTRIES_PATTERN = "Error : Countries cannot be Null or Empty and All must have exactly 2 Uppercase Letters.";
    public static final String ERROR_VALIDATION_LANGUAGE_PATTERN = "Error : Official Languages cannot be Null or Empty and All must have exactly 2 Lowercase Letters.";


}
