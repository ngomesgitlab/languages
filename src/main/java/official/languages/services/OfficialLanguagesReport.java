package official.languages.services;

import official.languages.common.functions.CountryLanguagesFunctions;
import official.languages.data.CountryLanguagesDTO;
import official.languages.data.CountryLanguagesReportDTO;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class OfficialLanguagesReport {

    protected CountryLanguagesReportDTO getCountryLanguagesReportDTO(Set<CountryLanguagesDTO> countryLanguagesDTOSet) {

        CountryLanguagesReportDTO countryLanguagesReportDTO = new CountryLanguagesReportDTO();

        //A - returns the number of countries in the world
        countryLanguagesReportDTO.setNumberCountriesInTheWorld(countryLanguagesDTOSet.size());

        //B - finds the country with the most official languages, where they officially speak German (de)
        countryLanguagesReportDTO.setCountryMostOfficialLanguagesIncludingGermany(
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguagesIncludingGermany.apply(countryLanguagesDTOSet));

        //C - that counts all the official languages spoken in the listed countries.
        countryLanguagesReportDTO.setNumberAllOfficialLanguages(
            CountryLanguagesFunctions.countAllOfficialLanguages.applyAsInt(countryLanguagesDTOSet));

        //D - to find the country with the highest number of official languages
        countryLanguagesReportDTO.setCountriesMostOfficialLanguage(
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguages.apply(countryLanguagesDTOSet));

        //E - to find the most common official language(s), of all countries
        countryLanguagesReportDTO.setMostCommonOfficialLanguage(
            CountryLanguagesFunctions.getMostCommonOfficialLanguage.apply(countryLanguagesDTOSet));

        return countryLanguagesReportDTO;

    }

}
