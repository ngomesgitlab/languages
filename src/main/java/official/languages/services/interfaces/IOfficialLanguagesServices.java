package official.languages.services.interfaces;

import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import official.languages.data.CountryLanguagesDTO;
import official.languages.data.CountryLanguagesReportDTO;

import java.util.Set;

public interface IOfficialLanguagesServices {
    CountryLanguagesReportDTO getCountryLanguagesReportDTO(Set<CountryLanguagesDTO> countryLanguagesDTOSet) throws InvalidLanguageException, InvalidCountryException;
}
