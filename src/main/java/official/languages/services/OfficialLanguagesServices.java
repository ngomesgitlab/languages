package official.languages.services;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import official.languages.common.validations.CountryLanguagesPatternValidations;
import official.languages.data.CountryLanguagesDTO;
import official.languages.data.CountryLanguagesReportDTO;
import official.languages.services.interfaces.IOfficialLanguagesServices;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@Slf4j
@RequiredArgsConstructor
public class OfficialLanguagesServices implements IOfficialLanguagesServices {

    private final OfficialLanguagesReport officialLanguagesReport;

    @Override
    public CountryLanguagesReportDTO getCountryLanguagesReportDTO(Set<CountryLanguagesDTO> countryLanguagesDTOSet) throws InvalidLanguageException, InvalidCountryException {

        /**
         * Validate Object
         */
        CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(countryLanguagesDTOSet);

        /**
         * Generate Report
         */
        return officialLanguagesReport.getCountryLanguagesReportDTO(countryLanguagesDTOSet);

    }

}
