package official.languages.testsupport.validations;

import official.languages.common.predicates.GenericPredicates;
import official.languages.data.CountryLanguagesDTO;
import official.languages.data.CountryLanguagesReportDTO;
import official.languages.data.ResponseDTO;
import org.junit.jupiter.api.Assertions;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

@SuppressWarnings("squid:S2187") //Being Asserted in Another Class
public class CountryLanguageValidationsTest {


    public static void validateCountryLanguagesDTO(
        CountryLanguagesDTO countryLanguagesDTO,
        String country,
        Set<String> languages) {

        Assertions.assertEquals(countryLanguagesDTO.getCountry(), country);

        Iterator<String> languagesIt = countryLanguagesDTO.getLanguages().iterator();

        languagesIt.forEachRemaining(languageSet -> {

            Assertions.assertTrue(languages.contains(languageSet));

        });

    }


    public static void validateCountryLanguagesReportDTO(
        CountryLanguagesReportDTO countryLanguagesReportDTOSet,
        int numberCountriesInTheWorld,
        int numberAllOfficialLanguages,
        List<String> countriesMostOfficialLanguagesIncludingGermany,
        List<String> countriesMostOfficialLanguages,
        List<String> mostCommonOfficialLanguages) {

        Assertions.assertEquals(numberCountriesInTheWorld, countryLanguagesReportDTOSet.getNumberCountriesInTheWorld());
        Assertions.assertEquals(numberAllOfficialLanguages, countryLanguagesReportDTOSet.getNumberAllOfficialLanguages());

        Assertions.assertEquals(
            countryLanguagesReportDTOSet.getCountryMostOfficialLanguagesIncludingGermany().size(), countriesMostOfficialLanguagesIncludingGermany.size());
        Assertions.assertTrue(
            countryLanguagesReportDTOSet.getCountryMostOfficialLanguagesIncludingGermany().containsAll(countriesMostOfficialLanguagesIncludingGermany));

        Assertions.assertEquals(
            countryLanguagesReportDTOSet.getCountriesMostOfficialLanguage().size(), countriesMostOfficialLanguages.size());
        Assertions.assertTrue(
            countryLanguagesReportDTOSet.getCountriesMostOfficialLanguage().containsAll(countriesMostOfficialLanguages));

        Assertions.assertEquals(
            countryLanguagesReportDTOSet.getMostCommonOfficialLanguage().size(), mostCommonOfficialLanguages.size());
        Assertions.assertTrue(
            countryLanguagesReportDTOSet.getMostCommonOfficialLanguage().containsAll(mostCommonOfficialLanguages));

    }


    public static void validateResponseDTO(ResponseDTO responseDTO) {

        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(responseDTO.getDetails()));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(responseDTO.getMessage()));

    }


}
