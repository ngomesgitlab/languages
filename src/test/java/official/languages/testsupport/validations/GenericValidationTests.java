package official.languages.testsupport.validations;

import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.function.BiConsumer;
@SuppressWarnings("squid:S2187")
public class GenericValidationTests {

    public static BiConsumer<List<String>, List<String>> validListContainsAllElements = (actualList, expectElements) -> {

        Assertions.assertEquals(actualList.size(), expectElements.size());
        Assertions.assertTrue(actualList.containsAll(expectElements));

    };

}
