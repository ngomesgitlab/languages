package official.languages.testsupport.examples;

import official.languages.data.CountryLanguagesDTO;

import java.util.HashSet;
import java.util.Set;

import static official.languages.testsupport.examples.AbstractTestClass.COUNTRY_VALID_US;
import static official.languages.testsupport.examples.AbstractTestClass.LANGUAGES_LANGUAGES_DE;
import static official.languages.testsupport.examples.AbstractTestClass.LANGUAGES_LANGUAGES_US;

public abstract class AbstractInvalidTestClass {

    /**
     * COUNTRIES
     */

    public static final String COUNTRY_INVALID_COUNTRY_NULL = null;
    public static final String COUNTRY_INVALID_COUNTRY_EMPTY = "";
    public static final String COUNTRY_INVALID_COUNTRY_BLANK = "    ";
    public static final String COUNTRY_INVALID_COUNTRY_ONE_CAPITAL_LETTER = "D";
    public static final String COUNTRY_INVALID_COUNTRY_ONE_NON_CAPITAL_LETTER = "d";
    public static final String COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER = "dd";
    public static final String COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER_2 = "Dd";
    public static final String COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER_3 = "dD";
    public static final String COUNTRY_INVALID_COUNTRY_THREE_CAPITAL_LETTER = "DDD";
    public static final String COUNTRY_INVALID_COUNTRY_THREE_NON_CAPITAL_LETTER = "ddd";


    /**
     * LANGUAGES
     */

    public static final String LANGUAGES_LANGUAGES_EMPTY = "";
    public static final String LANGUAGES_LANGUAGES_BLANK = "    ";
    public static final String LANGUAGES_LANGUAGES_ONE_CAPITAL_LETTER = "D";
    public static final String LANGUAGES_LANGUAGES_ONE_NON_CAPITAL_LETTER = "d";
    public static final String LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER = "DD";
    public static final String LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER_2 = "Dd";
    public static final String LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER_3 = "dD";
    public static final String LANGUAGES_LANGUAGES_THREE_CAPITAL_LETTER = "DDD";
    public static final String LANGUAGES_LANGUAGES_THREE_NON_CAPITAL_LETTER = "ddd";


    public class ObjectExamples {

        /**
         * LANGUAGES && COUNTRIES NULL AND EMPTY
         */

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_NULL = null;

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_NULL =
            CountryLanguagesDTO
                .builder()
                .languages(null)
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_EMPTY =
            CountryLanguagesDTO
                .builder()
                .languages(Set.of(LANGUAGES_LANGUAGES_EMPTY))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_NULL =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(null)
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_EMPTY_1 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(new HashSet<>())
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_EMPTY_2 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_EMPTY))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_BLANK =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_BLANK))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_NULL_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_NULL)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_EMPTY_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_EMPTY)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_BLANK_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_BLANK)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        /**
         * COUNTRIES WITH INVALID VALUES
         */

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_1_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_ONE_CAPITAL_LETTER)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_2_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_ONE_NON_CAPITAL_LETTER)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_3_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_4_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER_2)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_5_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_TWO_NON_CAPITAL_LETTER_3)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_6_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_THREE_CAPITAL_LETTER)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_7_LANGUAGE_VALID =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_INVALID_COUNTRY_THREE_NON_CAPITAL_LETTER)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        /**
         * LANGUAGES WITH INVALID VALUES
         */

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_1 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_ONE_CAPITAL_LETTER))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_2 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_ONE_NON_CAPITAL_LETTER))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_3 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_4 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER_2))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_5 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_TWO_CAPITAL_LETTER_3))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_6 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_THREE_CAPITAL_LETTER))
                .build();

        public final CountryLanguagesDTO INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_7 =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_THREE_NON_CAPITAL_LETTER))
                .build();

    }

}
