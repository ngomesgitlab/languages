package official.languages.testsupport.examples;

import official.languages.data.CountryLanguagesDTO;

import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class AbstractTestClass {

    /**
     * CountryLanguagesDTO
     */

    /**
     * COUNTRIES
     */

    public static final String COUNTRY_VALID_US = "US";
    public static final String COUNTRY_VALID_DE = "DE";
    public static final String COUNTRY_VALID_BE = "BE";
    public static final String COUNTRY_VALID_BR = "BR";

    /**
     * LANGUAGES
     */

    public static final String LANGUAGES_LANGUAGES_US = "us";
    public static final String LANGUAGES_LANGUAGES_DE = "de";
    public static final String LANGUAGES_LANGUAGES_PT = "pt";
    public static final String LANGUAGES_LANGUAGES_SP = "sp";
    public static final String LANGUAGES_LANGUAGES_FR = "fr";

    /**
     * CountryLanguagesReportDTO
     */

    public static final int NUMBER_COUNTRIES_IN_THE_WORLD = 1;
    public static final List<String> COUNTRIES_MOST_OFFICIAL_LANGUAGES_INCLUDING_GERMANY = List.of(COUNTRY_VALID_US, COUNTRY_VALID_DE);
    public static final int NUMBER_ALL_OFFICIAL_LANGUAGES = 2;
    public static final List<String> COUNTRIES_MOST_OFFICIAL_LANGUAGES = List.of(COUNTRY_VALID_US, COUNTRY_VALID_DE);
    public static final List<String> MOST_COMMON_OFFICIAL_LANGUAGES = List.of(COUNTRY_VALID_US, COUNTRY_VALID_DE);

    /**
     * ResponseDTO
     */

    public static final String RESPONSE_DTO_MESSAGE = "Message";
    public static final Map<String, String> RESPONSE_DTO_DETAILS_MAP = Map.of("KEY", "MAP");


    /**
     * Objects
     */

    public class ObjectExamples {

        public final CountryLanguagesDTO VALID_COUNTRY_LANGUAGE_DTO_BASIC =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();


        public final CountryLanguagesDTO VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_US)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_PT, LANGUAGES_LANGUAGES_SP))
                .build();

        public final CountryLanguagesDTO VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_BR)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_PT, LANGUAGES_LANGUAGES_SP))
                .build();

        public final CountryLanguagesDTO VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_DE)
                .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
                .build();

        public final CountryLanguagesDTO VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE =
            CountryLanguagesDTO
                .builder()
                .country(COUNTRY_VALID_BE)
                .languages(Set.of(LANGUAGES_LANGUAGES_FR, LANGUAGES_LANGUAGES_DE))
                .build();

    }

}
