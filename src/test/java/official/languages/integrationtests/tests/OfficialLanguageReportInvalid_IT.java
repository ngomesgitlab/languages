package official.languages.integrationtests.tests;

import lombok.extern.slf4j.Slf4j;
import official.languages.data.CountryLanguagesReportDTO;
import official.languages.integrationtests.actions.OfficialLanguagesHttpActions;
import official.languages.testsupport.examples.AbstractInvalidTestClass;
import official.languages.testsupport.examples.AbstractTestClass;
import official.languages.testsupport.validations.CountryLanguageValidationsTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.List;

/**
 * IT = Integration Tests
 */
@Slf4j
@ExtendWith(SpringExtension.class)
@SuppressWarnings("squid:S2699") //Being Asserted in Another Class
class OfficialLanguageReportInvalid_IT extends AbstractInvalidTestClass {

    private final ObjectExamples src = new ObjectExamples();

    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Return_A_Bad_Report_From_Generate_Report_Api_When_Sending_Invalid_Object() throws IOException {

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_EMPTY),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_NULL),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_EMPTY_1),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_EMPTY_2),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_BLANK),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_NULL_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_EMPTY_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_BLANK_LANGUAGE_VALID),
            400);

    }


    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Return_A_Bad_Report_From_Generate_Report_Api_When_Sending_Invalid_Country() throws IOException {

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_1_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_2_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_3_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_4_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_5_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_6_LANGUAGE_VALID),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_7_LANGUAGE_VALID),
            400);

    }

    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Return_A_Bad_Report_From_Generate_Report_Api_When_Sending_Invalid_Language() throws IOException {

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_1),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_2),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_3),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_4),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_5),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_6),
            400);

        OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_7),
            400);

    }

}
