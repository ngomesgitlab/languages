package official.languages.integrationtests.tests;

import lombok.extern.slf4j.Slf4j;
import official.languages.data.CountryLanguagesReportDTO;
import official.languages.integrationtests.actions.OfficialLanguagesHttpActions;
import official.languages.testsupport.examples.AbstractTestClass;
import official.languages.testsupport.validations.CountryLanguageValidationsTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * IT = Integration Tests
 */
@Slf4j
@ExtendWith(SpringExtension.class)
class OfficialLanguageReport_IT extends AbstractTestClass {

    private final ObjectExamples src = new ObjectExamples();

    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Get_CountryLanguagesReportDTO_With_Two_GermanCountries_And_Two_Non_GermanCountries() throws IOException {

        CountryLanguagesReportDTO countryLanguagesReportDTO = OfficialLanguagesHttpActions.generateCountryLanguagesReport(
             List.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE,
                 src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE),
            200);

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            4, //numberCountriesInTheWorld
            5, //numberAllOfficialLanguages
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE), //countriesMostOfficialLanguagesIncludingGermany
            List.of(COUNTRY_VALID_US, COUNTRY_VALID_BR), //countriesMostOfficialLanguages
            List.of(LANGUAGES_LANGUAGES_US)); //mostCommonOfficialLanguages

    }

    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Get_CountryLanguagesReportDTO_With_One_GermanCountries() throws IOException {

        CountryLanguagesReportDTO countryLanguagesReportDTO = OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE),
            200);

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            1, //numberCountriesInTheWorld
            2, //numberAllOfficialLanguages
            List.of(COUNTRY_VALID_DE), //countriesMostOfficialLanguagesIncludingGermany
            List.of(COUNTRY_VALID_DE), //countriesMostOfficialLanguages
            List.of(LANGUAGES_LANGUAGES_DE, LANGUAGES_LANGUAGES_US)); //mostCommonOfficialLanguages

    }



    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Get_CountryLanguagesReportDTO_With_Two_GermanCountries() throws IOException {

        CountryLanguagesReportDTO countryLanguagesReportDTO = OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE),
            200);

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            2, //numberCountriesInTheWorld
            3, //numberAllOfficialLanguages
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE), //countriesMostOfficialLanguagesIncludingGermany
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE), //countriesMostOfficialLanguages
            List.of(LANGUAGES_LANGUAGES_DE)); //mostCommonOfficialLanguages

    }


    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Get_CountryLanguagesReportDTO_With_One_Non_GermanCountries() throws IOException {

        CountryLanguagesReportDTO countryLanguagesReportDTO = OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE),
            200);

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            1, //numberCountriesInTheWorld
            3, //numberAllOfficialLanguages
            new ArrayList<>(), //countriesMostOfficialLanguagesIncludingGermany
            List.of(COUNTRY_VALID_US), //countriesMostOfficialLanguages
            List.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_PT, LANGUAGES_LANGUAGES_SP)); //mostCommonOfficialLanguages

    }


    /**
     * This is going to test the whole flow
     */
    @Test
    void should_Get_CountryLanguagesReportDTO_With_Two_Non_GermanCountries() throws IOException {

        CountryLanguagesReportDTO countryLanguagesReportDTO = OfficialLanguagesHttpActions.generateCountryLanguagesReport(
            List.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE),
            200);

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            2, //numberCountriesInTheWorld
            3, //numberAllOfficialLanguages
            new ArrayList<>(), //countriesMostOfficialLanguagesIncludingGermany
            List.of(COUNTRY_VALID_US, COUNTRY_VALID_BR), //countriesMostOfficialLanguages
            List.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_PT, LANGUAGES_LANGUAGES_SP)); //mostCommonOfficialLanguages

    }


}