package official.languages.integrationtests.connectors;

import official.languages.data.CountryLanguagesDTO;
import org.apache.http.HttpResponse;

import java.io.IOException;
import java.util.List;

import static official.languages.integrationtests.connectors.OfficialLanguagesEndpoint.GENERATE_REPORT;

public class OfficialLanguagesConnector {

    public static HttpResponse generateCountryLanguagesReport(
        List<CountryLanguagesDTO> countryLanguagesDTOList) throws IOException {

        String url = GENERATE_REPORT.getEndpoint();
        return new ApacheHttpConnector().httpPost(url, countryLanguagesDTOList);
    }


}
