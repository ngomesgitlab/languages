package official.languages.integrationtests.connectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OfficialLanguagesEndpoint {

    MAIN_URL("http://localhost:8088/officialLanguages"),

    GENERATE_REPORT(MAIN_URL.getEndpoint() + "/generateReport");

    private final String endpoint;

}
