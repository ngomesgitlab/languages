package official.languages.unittests.common.functions;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.functions.CountryLanguagesFunctions;
import official.languages.common.predicates.GenericPredicates;
import official.languages.testsupport.examples.AbstractTestClass;
import official.languages.testsupport.validations.GenericValidationTests;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;

@SuppressWarnings("squid:S2699") //Being Asserted in Another Class
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesFunctionsTest extends AbstractTestClass {

    private final ObjectExamples src = new ObjectExamples();

    /**
     * getCountryWithMostOfficialLanguagesIncludingGermany
     */

    @Test
    void validate_GetCountryWithMostOfficialLanguagesIncludingGermany_With_Two_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesIncludingGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguagesIncludingGermany.apply(
            Set.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            countryWithMostOfficialLanguagesIncludingGermany,
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE));

    }

    @Test
    void validate_GetCountryWithMostOfficialLanguagesIncludingGermany_With_Two_Non_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesIncludingGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguagesIncludingGermany.apply(
            Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE));

        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(countryWithMostOfficialLanguagesIncludingGermany));

    }

    @Test
    void validate_GetCountryWithMostOfficialLanguagesIncludingGermany_With_Two_GermanCountries_And_Two_Non_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesIncludingGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguagesIncludingGermany.apply(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE,
                    src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            countryWithMostOfficialLanguagesIncludingGermany,
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE));

    }


    /**
     * getCountryWithMostOfficialLanguagesIncludingGermany
     */

    @Test
    void validate_CountAllOfficialLanguages_With_Two_GermanCountries() {

        int allOfficialLanguages =
            CountryLanguagesFunctions.countAllOfficialLanguages.applyAsInt(
            Set.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        Assertions.assertEquals(3, allOfficialLanguages);

    }

    @Test
    void validate_CountAllOfficialLanguages_With_Two_Non_GermanCountries() {

        int allOfficialLanguages =
            CountryLanguagesFunctions.countAllOfficialLanguages.applyAsInt(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE));

        Assertions.assertEquals(3, allOfficialLanguages);

    }

    @Test
    void validate_CountAllOfficialLanguages_With_Two_GermanCountries_And_Two_Non_GermanCountries() {

        int allOfficialLanguages =
            CountryLanguagesFunctions.countAllOfficialLanguages.applyAsInt(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE,
                    src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        Assertions.assertEquals(5, allOfficialLanguages);

    }

    /**
     * getCountryWithMostOfficialLanguages
     */

    @Test
    void validate_GetCountryWithMostOfficialLanguages_With_Two_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesIncludingGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguages.apply(
                Set.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            countryWithMostOfficialLanguagesIncludingGermany,
            List.of(COUNTRY_VALID_DE, COUNTRY_VALID_BE));

    }

    @Test
    void validate_GetCountryWithMostOfficialLanguages_With_Two_Non_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesIncludingGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguages.apply(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            countryWithMostOfficialLanguagesIncludingGermany,
            List.of(COUNTRY_VALID_BR, COUNTRY_VALID_US));

    }

    @Test
    void validate_GetCountryWithMostOfficialLanguages_With_Two_GermanCountries_And_Two_Non_GermanCountries() {

        List<String> countryWithMostOfficialLanguagesNonGermany =
            CountryLanguagesFunctions.getCountryWithMostOfficialLanguages.apply(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE,
                    src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            countryWithMostOfficialLanguagesNonGermany,
            List.of(COUNTRY_VALID_US, COUNTRY_VALID_BR));

    }


    /**
     * getMostCommonOfficialLanguage
     */

    @Test
    void validate_GetMostCommonOfficialLanguage_With_Two_GermanCountries() {


        List<String> mostCommonOfficialLanguage =
            CountryLanguagesFunctions.getMostCommonOfficialLanguage.apply(
            Set.of(src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            mostCommonOfficialLanguage,
            List.of(LANGUAGES_LANGUAGES_DE));

    }

    @Test
    void validate_GetMostCommonOfficialLanguage_With_Two_Non_GermanCountries() {

        List<String> mostCommonOfficialLanguage =
            CountryLanguagesFunctions.getMostCommonOfficialLanguage.apply(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            mostCommonOfficialLanguage,
            List.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_PT, LANGUAGES_LANGUAGES_SP));

    }

    @Test
    void validate_GetMostCommonOfficialLanguage_With_Two_GermanCountries_And_Two_Non_GermanCountries() {

        List<String> mostCommonOfficialLanguage =
            CountryLanguagesFunctions.getMostCommonOfficialLanguage.apply(
                Set.of(src.VALID_COUNTRY_BR_WITHOUT_GERMANY_LANGUAGE, src.VALID_COUNTRY_US_WITHOUT_GERMANY_LANGUAGE,
                    src.VALID_COUNTRY_DE_WITH_GERMANY_LANGUAGE, src.VALID_COUNTRY_BE_WITH_GERMANY_LANGUAGE));

        GenericValidationTests.validListContainsAllElements.accept(
            mostCommonOfficialLanguage,
            List.of(LANGUAGES_LANGUAGES_US));

    }



}
