
package official.languages.unittests.common.validations;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import official.languages.common.validations.CountryLanguagesPatternValidations;
import official.languages.testsupport.examples.AbstractTestClass;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesPatternValidations_Valid_Test extends AbstractTestClass {

    private final ObjectExamples src = new ObjectExamples();

    @Test
    void should_not_thrown_exception_if_country_language_dto_is_valid() throws InvalidLanguageException, InvalidCountryException {

        CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(Set.of(src.VALID_COUNTRY_LANGUAGE_DTO_BASIC));

    }

}
