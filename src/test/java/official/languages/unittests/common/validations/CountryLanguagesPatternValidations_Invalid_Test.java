
package official.languages.unittests.common.validations;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import official.languages.common.validations.CountryLanguagesPatternValidations;
import official.languages.testsupport.examples.AbstractInvalidTestClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesPatternValidations_Invalid_Test extends AbstractInvalidTestClass {

    private final ObjectExamples src = new ObjectExamples();

    @Test
    void shouldThrowInvalidCountryObjectException() {

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(null);
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_NULL));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_EMPTY));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_LANGUAGE_EMPTY));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_NULL_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_EMPTY_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_BLANK_LANGUAGE_VALID));
        });


    }

    @Test
    void shouldThrowInvalidCountryPatternException() {

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_1_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_2_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_3_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_4_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_5_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_6_LANGUAGE_VALID));
        });

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_INVALID_7_LANGUAGE_VALID));
        });

    }


    @Test
    void shouldThrowInvalidLanguageObjectException() {

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_BLANK));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_EMPTY_2));
        });

    }

    @Test
    void shouldThrowInvalidLanguagePatternException() {

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_1));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_2));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_3));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_4));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_5));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_6));
        });

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            CountryLanguagesPatternValidations.validateCountryLanguagesDTOCollection(
                Set.of(src.INVALID_COUNTRY_LANGUAGE_DTO_COUNTRY_VALID_LANGUAGE_INVALID_7));
        });

    }

}
