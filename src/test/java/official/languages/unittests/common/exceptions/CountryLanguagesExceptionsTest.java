package official.languages.unittests.common.exceptions;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.exceptions.InvalidCountryException;
import official.languages.common.exceptions.InvalidLanguageException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static official.languages.common.constants.CountryLanguagesConstants.COUNTRY_PATTERN;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesExceptionsTest {

    @Test
    void should_throw_InvalidCountryException() {

        Assertions.assertThrows(InvalidCountryException.class, () -> {
            throw new InvalidCountryException(COUNTRY_PATTERN);
        });

    }

    @Test
    void should_throw_InvalidLanguageException() {

        Assertions.assertThrows(InvalidLanguageException.class, () -> {
            throw new InvalidLanguageException(COUNTRY_PATTERN);
        });

    }

}
