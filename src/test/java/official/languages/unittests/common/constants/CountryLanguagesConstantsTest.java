package official.languages.unittests.common.constants;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.predicates.GenericPredicates;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static official.languages.common.constants.CountryLanguagesConstants.COUNTRY_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.ERROR_DURING_GENERATED_OFFICIAL_LANGUAGE_REPORT;
import static official.languages.common.constants.CountryLanguagesConstants.ERROR_VALIDATION_COUNTRIES_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.ERROR_VALIDATION_LANGUAGE_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.LANGUAGE_PATTERN;
import static official.languages.common.constants.CountryLanguagesConstants.SPECIFIC_LANGUAGE;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesConstantsTest {

    @Test
    void should_be_valid_constants() {

        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(COUNTRY_PATTERN));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(LANGUAGE_PATTERN));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(SPECIFIC_LANGUAGE));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(ERROR_DURING_GENERATED_OFFICIAL_LANGUAGE_REPORT));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(ERROR_VALIDATION_COUNTRIES_PATTERN));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(ERROR_VALIDATION_LANGUAGE_PATTERN));

    }

}
