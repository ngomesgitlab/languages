package official.languages.unittests.common.predicates;

import official.languages.OfficialLanguagesApplication;
import official.languages.common.predicates.GenericPredicates;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class GenericPredicatesTests {

    /**
     * checkIfNullOrEmpty
     */

    @Test
    void should_Get_True_When_Object_Is_Empty() {

        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new ArrayList<>()));
        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new HashMap<>()));
        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(new HashSet<>()));
        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(""));

    }

    @Test
    void should_Get_True_When_Object_Is_Null() {

        Assertions.assertTrue(GenericPredicates.checkIfNullOrEmpty.test(null));

    }

    @Test
    void should_Get_False_When_Object_Has_Data() {

        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(List.of("Test")));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(Map.of("Test", "Test")));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(Set.of("Test")));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test("Test"));

    }




}
