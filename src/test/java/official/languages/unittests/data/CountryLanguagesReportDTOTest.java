package official.languages.unittests.data;

import official.languages.OfficialLanguagesApplication;
import official.languages.data.CountryLanguagesReportDTO;
import official.languages.testsupport.validations.CountryLanguageValidationsTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static official.languages.testsupport.examples.AbstractTestClass.COUNTRIES_MOST_OFFICIAL_LANGUAGES;
import static official.languages.testsupport.examples.AbstractTestClass.COUNTRIES_MOST_OFFICIAL_LANGUAGES_INCLUDING_GERMANY;
import static official.languages.testsupport.examples.AbstractTestClass.MOST_COMMON_OFFICIAL_LANGUAGES;
import static official.languages.testsupport.examples.AbstractTestClass.NUMBER_ALL_OFFICIAL_LANGUAGES;
import static official.languages.testsupport.examples.AbstractTestClass.NUMBER_COUNTRIES_IN_THE_WORLD;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesReportDTOTest {

    @Test
    void should_Get_Valid_CountryLanguagesReportDTO() {

        CountryLanguagesReportDTO countryLanguagesReportDTO = CountryLanguagesReportDTO
            .builder()
            .numberCountriesInTheWorld(NUMBER_COUNTRIES_IN_THE_WORLD)
            .countryMostOfficialLanguagesIncludingGermany(COUNTRIES_MOST_OFFICIAL_LANGUAGES_INCLUDING_GERMANY)
            .numberAllOfficialLanguages(NUMBER_ALL_OFFICIAL_LANGUAGES)
            .countriesMostOfficialLanguage(COUNTRIES_MOST_OFFICIAL_LANGUAGES)
            .mostCommonOfficialLanguage(MOST_COMMON_OFFICIAL_LANGUAGES)
            .build();

        CountryLanguageValidationsTest.validateCountryLanguagesReportDTO(
            countryLanguagesReportDTO,
            NUMBER_COUNTRIES_IN_THE_WORLD,
            NUMBER_ALL_OFFICIAL_LANGUAGES,
            COUNTRIES_MOST_OFFICIAL_LANGUAGES_INCLUDING_GERMANY,
            COUNTRIES_MOST_OFFICIAL_LANGUAGES,
            MOST_COMMON_OFFICIAL_LANGUAGES);

    }



}
