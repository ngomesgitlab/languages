package official.languages.unittests.data;

import official.languages.OfficialLanguagesApplication;
import official.languages.data.ResponseDTO;
import official.languages.testsupport.validations.CountryLanguageValidationsTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static official.languages.testsupport.examples.AbstractTestClass.RESPONSE_DTO_DETAILS_MAP;
import static official.languages.testsupport.examples.AbstractTestClass.RESPONSE_DTO_MESSAGE;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class ResponseDTOTest {

    @Test
    void should_Get_Valid_ResponseDTO() {

        ResponseDTO responseDTO = ResponseDTO
            .builder()
            .message(RESPONSE_DTO_MESSAGE)
            .details(RESPONSE_DTO_DETAILS_MAP)
            .build();

        CountryLanguageValidationsTest.validateResponseDTO(responseDTO);

    }

}
