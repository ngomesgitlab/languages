package official.languages.unittests.data;

import official.languages.OfficialLanguagesApplication;
import official.languages.data.CountryLanguagesDTO;
import official.languages.testsupport.examples.AbstractTestClass;
import official.languages.testsupport.validations.CountryLanguageValidationsTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Set;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {OfficialLanguagesApplication.class})
@ContextConfiguration
class CountryLanguagesDTOTest extends AbstractTestClass {


    @Test
    void should_Get_Valid_CountryLanguagesDTO() {

        CountryLanguagesDTO countryLanguagesDTO = CountryLanguagesDTO
            .builder()
            .country(COUNTRY_VALID_US)
            .languages(Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE))
            .build();

        CountryLanguageValidationsTest.validateCountryLanguagesDTO(
            countryLanguagesDTO,
            COUNTRY_VALID_US,
            Set.of(LANGUAGES_LANGUAGES_US, LANGUAGES_LANGUAGES_DE));

    }

}
